<?php
include "DbHelper.php";

$dir = "uploads";
if (!is_dir($dir)) {
    mkdir($dir);
}
$fileName = "1.jpg";
if (!empty($_POST)) {
    if (isset($_POST['title']) && isset($_POST['content'])&& isset($_POST['author'])&& (isset($_POST['category']) || isset($_POST['new_category']))) {
        $published_date = date('Y-m-d H:i:s');
        $pic = "$dir\\1.jpg";
        if (!empty($_FILES)) {
            if (isset($_FILES['picture'])) {
                if ($_FILES['picture']['error'] == UPLOAD_ERR_OK) {
                    $src = $_FILES['picture']['tmp_name'];
                    $fileName = $_FILES['picture']['name'];
                    $pic = "$dir/$fileName";
                    move_uploaded_file($src, $pic);
                }
            }
        }
        DbHelper::addPost($_POST,$fileName);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alex Blog Post</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<?php
include "navigation.php"
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-8">

            <div class="well">
                <h4>Add new post</h4>
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Title:
                        <input class="form-control" type="text" name="title"></label>
                        <br/>
                        <label>By:
                        <input class="form-control" type="text" name="author"></label>
                        <br/>
                        <label>Category:
                        <select class="form-control" name="category">
                            <option disabled>Choose category</option>
                            <?php
                            foreach (DbHelper::getCategories() as $row) {
                                echo "<option value='".$row->name."'>".$row->name."</option>";
                            }
                            ?>
                        </select></label>
                        <br/>
                        <label> or new category: <input class="form-control" type="text" name="new_category"></label>
                        <br/>
                        <label>Content:
                        <textarea class="form-control" rows="3" name="content"></textarea><br/></label>
                        <input type="file"  name="picture"/>
                    </div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->
    <?php
    include "footer.php";
    ?>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</body>

</html>
