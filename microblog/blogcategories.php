<?php
echo "             <div class=\"col-md-4\">

                <!-- Blog Search Well -->
                <form method=\"get\">
                <div class=\"well\">
                    <h4>Blog Search</h4>
                    <div class=\"input-group\">
                        <label>
                            <input type=\"text\" class=\"form-control\" name=search>
                        </label>
                        <span class=\"input-group-btn\">
                            <button class=\"btn btn-default\" type=\"submit\">
                                <span class=\"glyphicon glyphicon-search\"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                </div>
                </form>

                <!-- Blog Categories Well -->               <div class=\"well\">
                    <h4>Blog Categories</h4>
                    <div class=\"row\">
                        <div class=\"col-lg-6\">
                            <ul class=\"list-unstyled\">";

                            foreach (DbHelper::getCategories() as $row) {
                                echo "<li><a href='index.php?category=".$row->name."'>".$row->name."</a></li>";
                            }

 echo "                           </ul>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>                <!-- Side Widget Well -->
                <div class=\"well\">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>

            </div>";