<?php

include "Row.php";
/**
 * Created by PhpStorm.
 * User: Alwir
 * Date: 19.01.2017
 * Time: 15:47
 */

class DbHelper
{

    const CATEGORIES_TABLE_NAME="categories";
    const POSTS_TABLE_NAME="posts";
    const COMMENTS_TABLE_NAME="comments";

    private static function getDb(){

        $dsn = "sqlite:blog.sqlite";

        $db = new PDO($dsn);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if (!DbHelper::tableExists(self::CATEGORIES_TABLE_NAME,$db)) {
            DbHelper::createTableCategories($db);
        }

        if (!DbHelper::tableExists(self::POSTS_TABLE_NAME,$db)) {
            DbHelper::createTablePosts($db);
        }

        if (!DbHelper::tableExists(self::COMMENTS_TABLE_NAME,$db)) {
            DbHelper::createTableComments($db);
        }
        return $db;
    }

    private static function tableExists($table,PDO $db) {
        try {
            $result = $db->query("SELECT 1 FROM $table LIMIT 1");
        } catch (Exception $e) {
            return FALSE;
        }
        return $result !== FALSE;
    }

    private static function createTableCategories($db){
        $sql = "CREATE TABLE ".self::CATEGORIES_TABLE_NAME." (
        id integer PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL
        )";
        DbHelper::createTable($sql,$db);
    }

    private static function createTablePosts($db) {
        $sql = "CREATE TABLE ".self::POSTS_TABLE_NAME." (
        id integer PRIMARY KEY AUTOINCREMENT,
        title TEXT NOT NULL,
        author TEXT NOT NULL,
        category_id integer NOT NULL,
        content TEXT NOT NULL,
        picture TEXT NOT NULL,
        published_date TEXT NOT NULL
        )";
        DbHelper::createTable($sql,$db);
    }

    private static function createTableComments($db){
        $sql = "CREATE TABLE ".self::COMMENTS_TABLE_NAME." (
        id integer PRIMARY KEY AUTOINCREMENT,
        author TEXT NOT NULL,
        content TEXT NOT NULL,
        published_date TEXT NOT NULL,
        post_id integer NOT NULL
          )";
        DbHelper::createTable($sql,$db);
    }
    private static function createTable($sql,PDO $db){
        try {
            $db->exec($sql);
        }catch (PDOException $ex) {
            echo "<p style='color: red'><pre>" . print_r($ex) . "</pre></p>";
        }
    }

    public static function addPost($postData, $pic) {
        $categoryId=DbHelper::getCategoryId($postData[($postData['new_category'])?'new_category':'category']);
        $sql= "INSERT INTO ".self::POSTS_TABLE_NAME." (title, author, category_id, content, picture, published_date)values ('".
            $postData["title"]."','".
            $postData['author']."',".
            $categoryId.",'".
            $postData['content']."','".
            $pic."','".
            date('Y-m-d H:i:s')."')";
        DbHelper::addData($sql);
    }

    public static function getPosts($parameter){
        if(empty($parameter)) {
            $sql = "SELECT * FROM ".self::POSTS_TABLE_NAME." ORDER BY published_date DESC";
            return DbHelper::getData($sql);
        } else {
            if (isset($parameter['search']))
            {
                $sql = "SELECT * FROM ".self::POSTS_TABLE_NAME." WHERE title LIKE '".$parameter['search']."' ORDER BY published_date DESC";
                return DbHelper::getData($sql);
            }
            if (isset($parameter['category']))
            {
                $sql = "SELECT * FROM ".self::POSTS_TABLE_NAME.
                        " JOIN ".self::CATEGORIES_TABLE_NAME.
                        " ON (".self::CATEGORIES_TABLE_NAME.".id=".self::POSTS_TABLE_NAME.".category_id)".
                        " WHERE ".self::CATEGORIES_TABLE_NAME.".name LIKE '".$parameter['category']."'".
                        " ORDER BY published_date DESC";

                return DbHelper::getData($sql);
            }
        }
    }

    public static function getPost($postId) {
        $sql = "SELECT * FROM ".self::POSTS_TABLE_NAME." WHERE id=".$postId." ORDER BY published_date DESC";
        return DbHelper::getData($sql)[0];
    }

    private static function getData($sql) {
        $db=DbHelper::getDb();
        $pst = $db->prepare($sql);
        $pst->execute();
        return $pst->fetchAll(PDO::FETCH_CLASS, 'Row');
    }

    public static function addComment($postData) {
        $post_id = $_GET['postId'];
        $sql="INSERT INTO ".self::COMMENTS_TABLE_NAME."(author, content, published_date, post_id)
                values ('".$postData['author']."','".
            $postData['content']."','".
            date('Y-m-d H:i:s')."',".
            $post_id.")";
        DbHelper::addData($sql);
    }

    private static function addData($sql) {
        $db=DbHelper::getDb();
        $db->beginTransaction();
        try {
            $db->exec($sql);
            $db->commit();
        } catch (PDOException $ex) {
            $db->rollBack();
            echo "<p style='color: red'><pre>" . print_r($ex) . "</pre></p>";
        }
    }

    public static function getComments($postId) {
        $sql = "SELECT * FROM ".self::COMMENTS_TABLE_NAME." WHERE post_id = '".$postId."' order by published_date DESC ";
        return DbHelper::getData($sql);
    }

    public static function getCategories() {
        $db=DbHelper::getDb();
        $sql = "SELECT * FROM ".self::CATEGORIES_TABLE_NAME;
        $pst = $db->prepare($sql);
        $pst->execute();
        return $pst->fetchAll(PDO::FETCH_CLASS, 'Row');
    }

    private static function getCategoryId($categoryName) {
        $db=DbHelper::getDb();
        $sql = "SELECT * FROM ".self::CATEGORIES_TABLE_NAME." WHERE name LIKE '".$categoryName."'";
        $pst = $db->prepare($sql);
        $pst->execute();
        $respond=$pst->fetchAll(PDO::FETCH_CLASS, 'Row');
        if (!isset ($respond[0]->id)) {
            $sql = "INSERT INTO ".self::CATEGORIES_TABLE_NAME." (name) values ('".$categoryName."')";

            $db->beginTransaction();
            try {
                $db->exec($sql);
                $db->commit();
            } catch (PDOException $ex) {
                $db->rollBack();
                echo "<p style='color: red'><pre>" . print_r($ex) . "</pre></p>";
            }
            return DbHelper::getCategoryId($categoryName);
        }
        return $respond[0]->id;
    }



}