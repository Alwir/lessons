<?php
include "DbHelper.php";


if (!empty($_POST)) {
    if (isset($_POST['author']) && isset($_POST['content'])) {
        DbHelper::addComment($_POST);
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Alex Blog Post</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <?php
        include "navigation.php";
    ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">
                <!-- Blog Post -->
                <?php
                    $row=DbHelper::getPost($_GET['postId']);
                    echo "
                <!-- Title -->
                <h1>$row->title</h1>

                <!-- Author -->
                <p class='lead'>
                    by <a href='index.php'>$row->author</a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class='glyphicon glyphicon-time'></span>$row->published_date</p>

                <hr>

                <!-- Preview Image -->
                <img class='img-responsive' src='uploads/$row->picture' alt=''>

                <hr>

                <!-- Post Content -->
                <p class='lead'>$row->content</p>

                <hr>                   
                    
                    ";

                ?>
                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form method="post" role="form">
                        <div class="form-group">
                            <label>
                                <textarea class="form-control" rows="3" name ="content"></textarea>
                            </label>
                            <br>
                            <label>Author:
                            <input class="form-control" type="text" name="author"></label>
                        </div>
                        <button type="submit" class="btn btn-primary">New Comment</button>
                    </form>
                </div>

                <hr>

                <!-- Posted Comments -->

                <!-- Comment -->
                <?php
                foreach (DbHelper::getComments($_GET['postId']) as $row){
                    echo "<div class='media'>";
                    echo "<a class='pull-left' href='#'>";
                    echo "<img class='media-object' src='http://placehold.it/64x64' alt=''>";
                    echo "</a>";
                    echo "<div class='media-body'>";
                    echo "<h4 class='media-heading'>$row->author";
                    echo "<small> $row->published_date</small>";
                    echo "</h4>";
                    echo "$row->content";
                    echo "</div>";
                    echo "</div>";
                }
                ?>

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <?php
            include "blogcategories.php";
            ?>

        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <?php
        include "footer.php";
        ?>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
